<?php
class Router_model extends CI_Model{
 
    function router_list(){
        $hasil=$this->db->get('Router');
        return $hasil->result();
    }
 
    function save_router(){
        $data = array(
                'sapid'  => $this->input->post('sapid'), 
                'hostname'  => $this->input->post('hostname'), 
                'loopback' => $this->input->post('loopback'),
				'macaddress' => $this->input->post('macaddress'), 
            );
        $result=$this->db->insert('router',$data);
        return $result;
    }
 
    function update_router(){
        $sapid=$this->input->post('sapid');
        $hostname=$this->input->post('hostname');
	   $loopback=$this->input->post('loopback');
        $macaddress=$this->input->post('macaddress');
 
        $this->db->set('hostname', $hostname);
        $this->db->set('macaddress', $macaddress);
		$this->db->set('loopback', $loopback);
        $this->db->where('sapid', $sapid);
        $result=$this->db->update('router');
        return $result;
    }
 
    function delete_router(){
        $sapid=$this->input->post('sapid');
        $this->db->where('sapid', $sapid);
        $result=$this->db->delete('router');
        return $result;
    }
     
}
 